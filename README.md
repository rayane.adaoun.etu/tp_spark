# TP SPARK 
Dans cette article nous allons tous d'abord mettre en place un cluster hadoop sur 3 machines , pour que par la suite installer spark dessus et executer du code en scala de facon distribuée sur le cluster 

### Environnement du cluster 
- 3 serveurs Linux, distribution ubuntu 20.03 LTS , 4Go de Ram par machine 
- [hadoop-3.2.2 ](https://dlcdn.apache.org/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz)
- [spark-3.2.0](https://dlcdn.apache.org/spark/spark-3.2.0/spark-3.2.0-bin-hadoop3.2.tgz)

## Mise en place du cluster hadoop
> Le cluster est composé d'un master et de deux slaves
> - hadoop-master
> - hadoop-slave1 
> - hadoop-slave2

> La solution HADOOP sera composé principalement
> - **Hadoop Distributed File System (HDFS)**: un système de fichiers distribué qui stocke les données sur des machines de base, offrant une bande passante globale très élevée dans le cluster
> - **YARN** : un ordonnanceur pour gerer les applications au seins du cluster 
> - **Hadoop MapReduce** : une implementation  du modèle de programmation MapReduce pour le traitement des données à grande échelle.

### Etape 1: [Sur Toutes les machines]
- **installer pdsh**
```sh
$ sudo apt install pdsh
$ echo "export PDSH_RCMD_TYPE=ssh" >> ~/.bashrc
```
- Générer une paire de clés ssh 
```sh
$ ssh-keygen -t rsa -P ""
```
- Installer openjdk 8 
```sh
sudo apt update  
sudo apt install openjdk-8-jdk
java -version 
```
- Télécharger le binaire hadoop-3.2.2
```sh 
sudo wget -P ~ https://dlcdn.apache.org/hadoop/common/hadoop-3.2.2/hadoop-3.2.2.tar.gz
tar xzf hadoop-3.2.2.tar.gz
mv hadoop-3.2.2 hadoop
sudo mv hadoop /usr/local/hadoop
```
- changer la variable JAVA_HOME dans le fichier /usr/local/hadoop//etc/hadoop/hadoop-env.sh
```
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/
```
- Ajouter le chemin vers le binaire hadoop dans votre variable d'environnement PATH dans le fichier /etc/environment 
```
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/hadoop/bin:/usr/local/hadoop/sbin"
JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"
```
- Création  du  user hadoopuser 
```sh 
sudo adduser hadoopuser
sudo usermod -aG hadoopuser hadoopuser
sudo chown hadoopuser:root -R /usr/local/hadoop/
sudo chmod g+rwx -R /usr/local/hadoop/
sudo adduser hadoopuser sudo
```

- Sur chaque machine indiquer le nom de la machine dans le fichier /etc/hostname
```
sudo echo “hadoop-master” > /etc/hostame # pour la machine master 
sudo echo “hadoop-slave1” > /etc/hostame # pour la machine slave1 
sudo echo “hadoop-slave2” > /etc/hostame # pour la machine slave2
```
- Sur chaque machine associer l’ip de chaque instance à son nom dans le fichier /etc/hosts
- reboot toutes les machines 
```sh 
sudo reboot
```
### Etape 2: [Sur le master]
- sur la master passer en user hadoopuser et créer une paire de clés rsa 
```sh
hadoopuser@hadoop-master:~$ ssh-keygen -t rsa
```
- ajouter la clef publique du master sur chaque slave dans le fichier .ssh/authorized_keys 
-  ajouter la configuration suivante dans les fichiers 
     - /usr/local/hadoop/etc/hadoop/core-site.xml
```yaml  
<configuration>
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://hadoop-master:9000</value>
    </property>
</configuration>
```
     - /usr/local/hadoop/etc/hadoop/hdfs-site.xml
```yaml
<configuration>
    <property>
        <name>dfs.namenode.name.dir</name>
        <value>/usr/local/hadoop/data/nameNode</value>
    </property>
    <property>
        <name>dfs.datanode.data.dir</name>
        <value>/usr/local/hadoop/data/dataNode</value>
    </property>
    <property>
        <name>dfs.replication</name>
        <value>2</value>
    </property>
</configuration>
```
     - /usr/local/hadoop/etc/hadoop/workers
 ```yaml
 hadoop-slave1
 hadoop-slave2
 ```
- envoyer les modifications effectués sur les slaves . 
``` 
hadoopuser@hadoop-master:~$ scp /usr/local/hadoop/etc/hadoop/* hadoop-slave1:/usr/local/hadoop/etc/hadoop/
hadoopuser@hadoop-master:~$ scp /usr/local/hadoop/etc/hadoop/* hadoop-slave2:/usr/local/hadoop/etc/hadoop/
```
- Ajouter c’est variable hadoop et spark pour la suite  dans le fichier ~/.bashrc de chaque machines 
```
export PDSH_RCMD_TYPE=ssh
export HADOOP_HOME="/usr/local/hadoop"
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export HADOOP_HDFS_HOME=$HADOOP_HOME
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_YARN_HOME=$HADOOP_HOME

export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
export SPARK_HOME=/usr/local/spark
export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native:$LD_LIBRARY_PATH
```
```sh 
source ~/.bashrc 
source /etc/environment 
```
- Démarrer HDFS
    ```
     start-dfs.sh
    ```
- Vérifier que HDFS à bien démarrer 

Sur le master lancer la commande jps et vous devez avoir ce résultat 
```
2506 NameNode
24125 Jps
2766 SecondaryNameNode
```
et sur les slaves 
```
1808 DataNode
2042 Jps
```

- Rendu graphique sur le navigateur : http://hadoop-master:9870
![image info](./images/hdfs.png)

### Etape 3: mise en place de Yarn 
- Ajouter la configuration suivante dans le fichier /usr/local/hadoop/etc/hadoop/yarn-site.xml
```yaml 
<configuration>

<!-- Site specific YARN configuration properties -->
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>hadoop-master</value>
    </property>
    <property>
                <name> yarn.scheduler.maximum-allocation-mb </name>
                <value> 1536 </value>
        </property>
</configuration>
```

- Démarrer yarn 
```sh 
start-yarn.sh
```
- Rendu graphique sur le navigateur : http://hadoop-master:8088/cluster
![image info](./images/hadoop.png)

**Bravo , vous avez un cluster Hadoop mis en route avec 2 slaves.**

## Mise en place de spark sur le cluster hadoop

### ETAPE 1 [Sur toutes les machines]

- Telecharger le binaire spark-3.2.0
```sh 
wget https://dlcdn.apache.org/spark/spark-3.2.0/spark-3.2.0-bin-hadoop3.2.tgz
tar -xzf spark-3.2.0-bin-hadoop3.2.tgz
mv spark-3.2.0-bin-hadoop3.2.tgz spark 
```
- Transferer le dossier dans /usr/local et mettre à jour les variables d'environnement
```
sudo mv spark /usr/local/spark 
source ~/.bashrc
```
Mettre à jour la variable PATH dans /etc/environment
```
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/lib/jvm/java-8-openjdk-amd64/:/usr/local/hadoop/bin:/usr/local/hadoop/sbin:/usr/local/spark/bin:/usr/local/spark/sbin"
JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/jre"
PDSH_RCMD_TYPE=ssh
```
- Renommer le template par defaut de spark 
```sh
 mv $SPARK_HOME/conf/spark-defaults.conf.template $SPARK_HOME/conf/spark-defaults.conf
 ```
- Ajouter la config suivante dans le fichier $SPARK_HOME/conf/spark-defaults.conf
```
spark.master                       yarn
spark.eventLog.enabled             true
spark.eventLog.dir                 hdfs://hadoop-master:9000/spark-logs
spark.driver.memory                512m
spark.yarn.am.memory               512m
spark.executor.memory              512m
spark.history.provider             org.apache.spark.deploy.history.FsHistoryProvider
spark.history.fs.logDirectory      hdfs://hadoop-master:9000/spark-logs
spark.history.fs.update.interval   10s
spark.history.ui.port              18080
```
- Ajouter la config suivante dans le fichier $HADOOP_CONF_DIR/yarn-site.xml
```
<configuration>

<!-- Site specific YARN configuration properties -->
	<property>
		<name>yarn.resourcemanager.hostname</name>
		<value>hadoop-master</value>
	</property>
	<property>
                <name> yarn.scheduler.maximum-allocation-mb </name>
                <value> 1536 </value>
        </property>
</configuration>
```
- Creer le dossier de logs spark  sur hdfs (executer la command sur la machine hadoop-master)
```
hdfs dfs -mkdir /spark-logs
```
- Lancer spark sur la machine hadoop-master
```
start-all.sh
```
- Faire un premier test avec le fichier exemple fourni par spark 
```
spark-submit --deploy-mode client --class org.apache.spark.examples.SparkPi $SPARK_HOME/examples/jars/spark-examples_2.12-3.2.0.jar 10
```
- Si la commande c'est lancé sans erreur, vous trouverez un rendu de l'application dans spark history à l'adresse http://hadoop-master:18080
![image info](./images/history.png)
- Si on cherche un peu dans les taches , on voit bien qu'elle se lance sur les 2 slaves.
![image info](./images/tasks.png)

## Deploiement du code scala sur le cluster (depuis le master)
- Il faut dans un premier temps mettre le fichier de data csv sur le cluster avec hdfs dans le dossier inputs 
```
hdfs dfs -mkdir inputs
hdfs dfs -put equipementsculturelsvilleneuvedascq.csv inputs
```
![image info](./images/fichier-hdfs.png)
- Nous pouvons maintenant lancer notre code scala via le spark-shell
```
spark-shell -i code.scala
```
![image info](./images/spark-shell.png)
- on voit bien que le code s'execute et affiche le resultat , nous allons verifier le spark history pour s'assurer que ce dernier c'est bien lancer sur le cluster et non pas en local 
![image info](./images/resultat-history.png)
- Resultat sur hadoop 
![image info](./images/resultat-hadoop.png)
- On voit bien au vu des differnts essaie que le code s'execute bien sur les 2 slaves , mais aussi lors de l'execution d'une meme application ,certains jobs sont executés sur un slave et d'autre sur l'autre.

![image info](./images/resultat-slave1.png)
![image info](./images/resulat-slave2.png)

